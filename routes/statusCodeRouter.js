const express = require('express');
const router = express.Router();

router.get('/status/:status_code', (req, res) => {
    const statusCode = parseInt(req.params.status_code);
    if (statusCode) {
        res.sendStatus(statusCode);
    } else {
        res.status(400).json({ message: "Not a valid status code" })
    }

});

router.get('/status', (req, res) => {
    res.status(400).json({message:"Not a valid url"})
})

module.exports = router;
