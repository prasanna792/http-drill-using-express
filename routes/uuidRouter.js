const express = require('express');
const router = express.Router();
const { v4: uuidv4 } = require('uuid');


router.get('/uuid', (req, res) => {
    const uuid = uuidv4();
    if (uuid) {
        res.json({ uuid });
    } else {
        res.sendStatus(400)
    }

});

module.exports = router;
