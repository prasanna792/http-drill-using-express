const express = require('express');
const router = express.Router();
const jsonData = require('../public/data.json')


router.get('/json', (req, res) => {
    if (jsonData) {
        res.json(jsonData);
    } else {
        res.sendStatus(404)
    }

});


module.exports = router;
