const express = require('express');
const router = express.Router();


router.get('/delay/:delay_in_seconds', (req, res) => {
    const delaySeconds = parseInt(req.params.delay_in_seconds);
    if (delaySeconds) {
        setTimeout(() => {
            res.sendStatus(200);
        }, delaySeconds * 1000);
    } else {
        res.sendStatus(400)
    }

});

router.get('/delay', (req, res) => {
    res.status(400).json({ message: "Not a valid url" })
})



module.exports = router;
