const express = require('express');
const uuidRouter = require('./routes/uuidRouter');
const path = require('path');
const statusCodeRouter = require('./routes/statusCodeRouter');
const jsonDataRouter = require('./routes/jsonDataRouter');
const delayStatusRouter = require('./routes/delayStatusRouter');
const rootRouter = require('./routes/rootRouter')

const app = express();
const PORT = process.env.PORT || 8000;

app.use(express.json());

app.use(rootRouter);
app.use('/html', express.static(path.join(__dirname, 'public')))
app.use(jsonDataRouter);
app.use(uuidRouter);
app.use(statusCodeRouter);
app.use(delayStatusRouter);


app.listen(PORT, () => {
    console.log(`Server is running on ${PORT}`);
});
